<?php
namespace Drupal\School\Controller;

class SchoolController {

  public function index(): array
  {
    return [
      '#markup' => 'Hello world, School!',
    ];
  }
}
