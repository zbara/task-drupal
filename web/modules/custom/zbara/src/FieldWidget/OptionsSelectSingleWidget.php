<?php

namespace Drupal\zbara\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'options_select_single' widget.
 *
 * @FieldWidget(
 *   id = "options_select_single",
 *   label = @Translation("Select list (Single)"),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   },
 *   multiple_values = FALSE
 * )
 */
class OptionsSelectSingleWidget extends OptionsSelectWidget
{

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
  {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['#multiple'] = FALSE;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state)
  {

    $new_values = [];

    foreach ($values as $key => $value) {
      $new_values = $values[$key]['_original_delta'];
      foreach ($values as $key) {
        if (is_array($new_values['target_id']) && !empty($new_values['target_id'])) {
          unset($values[$new_values][$key][0]);
          $values[$new_values][$key] = $new_values[0]['target_id'];
        }
      }
    }
    return $new_values;
  }
}
