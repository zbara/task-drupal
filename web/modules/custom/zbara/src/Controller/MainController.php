<?php

namespace Drupal\Zbara\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Zbara\Services\ApiWeather;
use Psr\Container\ContainerInterface;

/**
 * Provides the random and weather .
 */
class MainController extends ControllerBase
{
  private ApiWeather $apiWeather;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): MainController
  {
    return new static (
      $container->get('zbara.weather')
    );
  }

  /**
   * @param ApiWeather $weather_api
   */
  public function __construct(ApiWeather $weather_api)
  {
    $this->apiWeather = $weather_api;
  }

  /**
   * @title Рандомная строка.
   * @url: /zbara/random
   * @return array
   * @throws \Exception
   */
  public function random(): array
  {
    $random = random_int(1, 30);

    return [
      '#theme' => 'zbara_random',
      '#random' => $random
    ];
  }

  /**
   * @title Вывод информации о погоде из сервиса.
   * @url: /zbara/weather
   * @return array
   */
  public function weather(): array
  {
    $weather = $this->apiWeather->getWeather();

    return [
      '#theme' => 'zbara_weather',
      '#temperature' => $weather['temperature'],
      '#description' => $weather['description']
    ];
  }
}
