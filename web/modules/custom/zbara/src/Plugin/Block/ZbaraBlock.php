<?php
namespace Drupal\Zbara\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "hello_block",
 *   admin_label = @Translation("Hello block"),
 *   category = @Translation("Hello World"),
 * )
 */

class ZbaraBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    return [
      '#markup' => $this->t('Hello, World!'),
    ];
  }
}
